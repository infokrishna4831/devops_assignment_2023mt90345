package com.lg.gram.springboot.micro.ratingsdataservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RatingsDataServiceApplication {

	public static void main(String[] args) {
	//Updating this file
	SpringApplication.run(RatingsDataServiceApplication.class, args);

	}

}
